import test from 'ava';

import { getVisibleMarkers } from '../../src/utils/coordinates';

test('utils/coordinatesSpec.js: getVisibleMarkers()', t => {
  let dataPointInside = {
    lat: 52.1,
    lng: 5.1
  };
  let dataPointOutside = {
    lat: 51,
    lng: 4
  };
  let dataPointOnBorder = {
    lat: 52,
    lng: 5
  };
  let dataSetOne = [
    dataPointInside,
    dataPointOutside
  ];
  let dataSetTwo = [
    dataPointOnBorder
  ];
  let dataSetThree = [
    dataPointOutside
  ];

  let bounds = {
    getNorthEast: () => {
      return {
        lat: () => {
          return 53
        },
        lng: () => {
          return 6
        }
      };
    },
    getSouthWest: () => {
      return {
        lat: () => {
          return 52
        },
        lng: () => {
          return 5
        }
      };
    }
  };

  t.true(Array.isArray(getVisibleMarkers(dataSetOne, bounds)));
  t.is(getVisibleMarkers(dataSetOne, bounds).length, 1);
  t.is(getVisibleMarkers(dataSetTwo, bounds).length, 1);
  t.is(getVisibleMarkers(dataSetThree, bounds).length, 0);
});
