import test from 'ava';
import Promise from 'promise';
import proxyquire from 'proxyquire';

import { BaseStore } from 'fluxible/addons';
import { createMockActionContext } from 'fluxible/utils';

import ApplicationStore from '../../src/stores/ApplicationStore';

let LoadComments = proxyquire('../../src/actions/LoadComments', {
  '../services/getComments': () => {
    return Promise.resolve(['1']);
  }
});

class MockStore extends BaseStore {
    constructor (dispatcher) {
        super(dispatcher);
        this.cache = {
          'id2': ['cached data']
        };
    }
    getCommentsById (id) {
        return this.cache[id];
    }
}
MockStore.storeName = 'ApplicationStore';

test('actions/LoadCommentsSpec.js: dispatch proper event with data', t => {
  let actionContext = createMockActionContext({
    stores: [MockStore]
  });

  return LoadComments(actionContext, {
      id: 'id'
    })
    .then(() => {
      t.is(1, actionContext.dispatchCalls.length);
      t.is('COMMENTS_LOADED', actionContext.dispatchCalls[0].name);
      t.is('id', actionContext.dispatchCalls[0].payload.id);
      t.is(1, actionContext.dispatchCalls[0].payload.data.length);
    });
});

test('actions/LoadCommentsSpec.js: should return cached version', t => {
  let actionContext = createMockActionContext({
    stores: [MockStore]
  });

  return LoadComments(actionContext, {
      id: 'id2'
    })
    .then((data) => {
      t.is(0, actionContext.dispatchCalls.length);
      t.is('cached data', data[0]);
    });
});
