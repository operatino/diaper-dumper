/**
 * Get filtered array with dots inside defined cord square
 *
 * @param {Array} data - Raw data array to filter
 * @param {Object} bounds - Google Maps bounds object (getBounds())
 *
 * @returns {Array} Result of the server request
 */
export function getVisibleMarkers(data, bounds) {
  let northEast = bounds.getNorthEast();
  let southWest = bounds.getSouthWest();

  return data.filter(item => {
    return (
      southWest.lat() <= item.lat &&
      item.lat <= northEast.lat() &&
      southWest.lng() <= item.lng &&
      item.lng <= northEast.lng()
    );
  });
}
