import BaseStore from 'fluxible/addons/BaseStore';
import RouteStore from './RouteStore';

class ApplicationStore extends BaseStore {
  constructor(dispatcher) {
    super(dispatcher);
    this.pageTitle = '';
    this.mapData = [];
    this.commentsData = {};
  }

  /**
   * Set page title depending on a route
   *
   * @param {Object} currentRoute
   *
   */
  handlePageTitle(currentRoute) {
    this.dispatcher.waitFor(RouteStore, () => {
      if (currentRoute && currentRoute.title) {
        this.pageTitle = currentRoute.title;
        this.emitChange();
      }
    });
  }

  /**
   * Save list of map points
   *
   * @param {Array} data
   *
   */
  setMapData(data) {
    this.mapData = data;
    this.emitChange();
  }

  /**
   * Store data from server by id
   *
   * @param {Object} payload
   * @param {String} payload.id
   * @param {Array} payload.data
   *
   */
  setComments(payload) {
    this.commentsData[payload.id] = payload.data;
    this.emitChange();
  }

  /**
   * Get cached comments by id
   *
   * @param {String} id
   *
   * @returns {Array<Object>} Returns list of comments
   */
  getCommentsById(id) {
    return this.commentsData[id];
  }

  getPageTitle() {
    return this.pageTitle;
  }

  getMapData() {
    return this.mapData;
  }

  dehydrate() {
    return {
      pageTitle: this.pageTitle,
      mapData: this.mapData,
      commentsData: this.commentsData
    };
  }

  rehydrate(state) {
    this.pageTitle = state.pageTitle;
    this.mapData = state.mapData;
    this.commentsData = state.commentsData;
  }
}

ApplicationStore.storeName = 'ApplicationStore';
ApplicationStore.handlers = {
  'NAVIGATE_SUCCESS': 'handlePageTitle',
  'COMMENTS_LOADED': 'setComments',
  'LOAD_INITIAL_DATA': 'setMapData'
};

export default ApplicationStore;
