var hook = require('css-modules-require-hook');

require('babel/register');

hook({
  generateScopedName: '[name]__[local]___[hash:base64:5]',
  prepend: [
    require('postcss-nested')()
  ]
});

module.exports = require('./server');
