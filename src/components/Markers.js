import React from 'react';
import Marker from 'react-google-maps/lib/Marker';
import InfoWindow from 'react-google-maps/lib/InfoWindow';
import { default as MarkerClusterer } from 'react-google-maps/lib/addons/MarkerClusterer';

class Markers extends React.Component {
  static defaultProps = {
  };

  constructor(props) {
    super(props);
  }

  handleClick(marker) {
    if (typeof this.props.onMarkerClick === 'function') {
      this.props.onMarkerClick(marker);
    }
  }

  renderPopup(ref, marker) {
    return (
      <InfoWindow
        key={`${ref}_info_window`}
        onCloseclick={this.handleClick.bind(this, null)} >
        <div>{marker.name}, {marker.address}</div>
      </InfoWindow>
    );
  }

  renderMarkers() {
    return this.props.markers.map((marker, index) => {
      const ref = `marker_${index}`;

      return (
        <Marker
          position={{lat: marker.lat,
lng: marker.lng}}
          key={index}
          ref={ref}
          onClick={this.handleClick.bind(this, marker)}
        >
          {this.props.selectedItem === marker.id ? this.renderPopup(ref, marker) : null}
        </Marker>
      );
    });
  }

  render() {
    return (
      <MarkerClusterer
        mapHolderRef={this.props.mapHolderRef}
        averageCenter
        enableRetinaIcons
        gridSize={ 30 }
      >
        {this.renderMarkers()}
      </MarkerClusterer>
    );
  }
}

export default Markers;
