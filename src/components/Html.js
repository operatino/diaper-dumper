import React from 'react';
import ApplicationStore from '../stores/ApplicationStore';

class Html extends React.Component {
  renderStyles() {
    if (process.env.NODE_ENV !== 'production') {
      return null;
    }

    return (
      <link rel='stylesheet' href='/public/style.css' />
    );
  }

  render() {
    return (
      <html>
        <head>
          <meta charSet='utf-8'/>
          <title>{this.props.context.getStore(ApplicationStore).getPageTitle()}</title>
          <meta name='viewport' content='width=device-width'/>
          <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places'></script>
          {this.renderStyles()}
        </head>
        <body>
          <div id='app' dangerouslySetInnerHTML={{__html: this.props.markup}}></div>
          <script dangerouslySetInnerHTML={{__html: this.props.state}}></script>
          <script src={'/public/' + this.props.clientFile}></script>
        </body>
      </html>
    );
  }
}

export default Html;
