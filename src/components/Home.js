/* global document */

import React from 'react';
import Map from './Map';
import List from './List';
import Search from './Search';
import ApplicationStore from '../stores/ApplicationStore';
import connectToStores from 'fluxible-addons-react/connectToStores';
import { getVisibleMarkers } from '../utils/coordinates';
import styles from './Home.css';

class Home extends React.Component {
  static contextTypes = {
    getStore: React.PropTypes.func
  };

  constructor(props) {
    super(props);

    let defaultZoom = 8;

    if (typeof document !== 'undefined' && document.documentElement.clientWidth < 1023) {
      defaultZoom = 7;
    }

    this.defaultCenter = {
      lat: 52.128519,
      lng: 4.935483
    };

    this.defaultZoom = defaultZoom;
    this.zoomOnActive = 13;

    this.state = {
      visibleItems: this.props.data,
      selectedItem: null
    };
  }

  onBoundsChange(bounds) {
    let visibleItems = getVisibleMarkers(this.props.data, bounds);

    this.setState({
      visibleItems: visibleItems
    });
  }

  handleSearch(suggest) {
    if (this.map) {
      this.map.setCenter(suggest.location);
      this.map.setZoom(this.zoomOnActive);
    }
  }

  handleSearchChange(value) {
    if (value === '' && this.map) {
      this.map.setCenter(this.defaultCenter);
      this.map.setZoom(this.defaultZoom);

      this.onBoundsChange(this.map.getBounds());
    }
  }

  handleMarkerClick(marker) {
    let selectedItem = marker && marker.id ? marker.id : '';

    this.setState({
      selectedItem: selectedItem
    });
  }

  handleListItemClick(marker) {
    if (this.map) {
      if (this.map.getZoom() < this.zoomOnActive) {
        this.map.setZoom(this.zoomOnActive);
      }

      this.map.setCenter({
        lat: marker.lat,
        lng: marker.lng
      });
    }

    this.setState({
      selectedItem: marker.id
    });
  }

  getMap(map) {
    this.map = map;
  }

  render() {
    return (
      <div className={styles.main}>
        <Search
          onSearch={this.handleSearch.bind(this)}
          onChange={this.handleSearchChange.bind(this)}
        />
        <div className={styles.left}>
          <List
            items={this.state.visibleItems}
            selectedItem={this.state.selectedItem}
            onItemClick={this.handleListItemClick.bind(this)}
          />
        </div>
        <div className={styles.right}>
          <Map
            getMap={this.getMap.bind(this)}
            defaultCenter={this.defaultCenter}
            defaultZoom={this.defaultZoom}
            onBoundsChange={this.onBoundsChange.bind(this)}
            markers={this.props.data}
            selectedItem={this.state.selectedItem}
            onMarkerClick={this.handleMarkerClick.bind(this)}
          />
        </div>
      </div>
    );
  }
}

Home = connectToStores(Home, ['ApplicationStore'], (context, props) => ({
  data: context.getStore(ApplicationStore).getMapData()
}));

export default Home;
