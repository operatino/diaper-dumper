import React from 'react';

import styles from './Comments.css';

class CommentItem extends React.Component {
  static defaultProps = {
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.item}>
        <div>Rating: {this.props.positive ? 'positive' : 'negative'}</div>
        <div>Comments: {this.props.comment}</div>
      </div>
    );
  }
}

export default CommentItem;
