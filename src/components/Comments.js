import React from 'react';

import ApplicationStore from '../stores/ApplicationStore';
import LoadComments from '../actions/LoadComments';
import CommentItem from './CommentItem';
import connectToStores from 'fluxible-addons-react/connectToStores';

import styles from './Comments.css';

class Comments extends React.Component {
  static defaultProps = {
    comments: null
  };

  static contextTypes = {
    executeAction: React.PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      shown: false
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !this.props.comments || (nextProps.comments && this.props.comments.length !== nextProps.comments.length) || this.state.shown !== nextState.shown;
  }

  handleClick() {
    if (this.state.shown) {
      this.setState({shown: false});
    } else {
      this.setState({shown: true});

      if (!this.props.comments) {
        this.context.executeAction(LoadComments, {
          id: this.props.id
        });
      }
    }
  }

  renderRating(comments) {
    let positive = 0;
    let negative = 0;

    comments.forEach(item => {
      if (item.positive) {
        positive++;
      } else {
        negative++;
      }
    });

    return (
      <div className={styles.rating}>
        Positive: {positive}, Negative: {negative}
      </div>
    );
  }

  renderComments() {
    if (this.state.shown) {
      if (this.props.comments) {
        if (this.props.comments.length === 0) {
          return (
            <div>No comments</div>
          );
        } else {
          return (
            <div>
              {this.renderRating(this.props.comments)}
              {this.props.comments.map((item, index) => {
                return <CommentItem {...item} key={index}/>;
              })}
            </div>
          );
        }
      } else {
        return (
          <div>Loading...</div>
        );
      }
    } else {
      return null;
    }
  }

  render() {
    let buttonText = this.state.shown ? 'HideComments' : 'Show Comments';

    return (
      <div className={styles.main}>
        <button onClick={this.handleClick.bind(this)}>{buttonText}</button>
        <div className={styles.list}>
          {this.renderComments()}
        </div>
      </div>
    );
  }
}

Comments = connectToStores(Comments, ['ApplicationStore'], (context, props) => ({
  comments: context.getStore(ApplicationStore).getCommentsById(props.id)
}));

export default Comments;
