import React from 'react';
import cx from 'classnames';
import { NavLink } from 'fluxible-router';
import styles from './Nav.css';

class Nav extends React.Component {
  render() {
    const selected = this.props.currentRoute;
    const links = this.props.links;

    const linkHTML = Object.keys(links).map((name) => {
      var classes = cx(
        styles.item,
        {
          [styles.itemSelected]: selected && selected.name === name
        }
      );
      var link = links[name];

      return (
        <li className={classes} key={link.path}>
          <NavLink className={styles.link} routeName={link.page}>{link.title}</NavLink>
        </li>
      );
    });

    return (
      <ul className={styles.main}>
        {linkHTML}
      </ul>
    );
  }
}

Nav.defaultProps = {
  selected: null,
  links: {}
};

export default Nav;
