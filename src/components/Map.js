import React from 'react';
import GoogleMapLoader from 'react-google-maps/lib/GoogleMapLoader';
import GoogleMap from 'react-google-maps/lib/GoogleMap';
import styles from './Map.css';

import Markers from './Markers';

import debounce from 'lodash/debounce';

class Map extends React.Component {
  static defaultProps = {
    maxOpenedPopups: 3,
    debounceTimeout: 500
  };

  constructor(props) {
    super(props);

    this.popupOpenHistory = [];
    this.mapPassed = false;
  }

  handleBoundsChanged() {
    if (typeof this.props.onBoundsChange === 'function') {
      this.props.onBoundsChange(this._googleMapComponent.getBounds());
    }
  }

  handleZoomChanged() {
    if (typeof this.props.onZoomChanged === 'function') {
      this.props.onZoomChanged(this._googleMapComponent.getZoom());
    }
  }

  render() {
    return (
      <GoogleMapLoader
        containerElement = {
          <div className={styles.main} />
        }
        googleMapElement = {
          <GoogleMap
            ref={(it) => {
              if (this.mapPassed) {
                return;
              }

              this._googleMapComponent = it;

              if (typeof this.props.getMap === 'function') {
                this.props.getMap(it.props.map);
              }

              this.mapPassed = true;
            }}
            className='google-map'
            onBoundsChanged={debounce(this.handleBoundsChanged.bind(this), this.props.debounceTimeout)}
            onZoomChanged={this.handleZoomChanged.bind(this)}
            defaultCenter={this.props.defaultCenter}
            defaultZoom={this.props.defaultZoom}
          >
            <Markers
              markers={this.props.markers}
              onMarkerClick={this.props.onMarkerClick}
              selectedItem={this.props.selectedItem}
            />
          </GoogleMap>
        }
      />
    );
  }
}

export default Map;
