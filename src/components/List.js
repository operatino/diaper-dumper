import React from 'react';

import ListItem from './ListItem';
import styles from './List.css';

class List extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ul className={styles.main}>
        {this.props.items.map((item, index) => {
          let props = {
            key: index,
            index,
            selectedItem: this.props.selectedItem,
            onItemClick: this.props.onItemClick
          };

          return <ListItem {...item} {...props} />;
        })}
      </ul>
    );
  }
}

export default List;
