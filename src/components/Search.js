import React from 'react';
import Geosuggest from 'react-geosuggest';

import styles from './Search.css';

class Search extends React.Component {
  static defaultProps = {
    searchCountry: 'nl',
    placeholder: 'Type in your postal code',
    filterSuggestionsBy: 'postal_code',
    defaultSearchBounds: {
      southWest: {
        latitude: 50.77465,
        longitude: 2.80419
      },
      northEast: {
        latitude: 53.53702,
        longitude: 7.55029
      }
    },
    autoActivateFirstSuggest: true
  };

  constructor(props) {
    super(props);
  }

  handleSeggestSelect(suggest) {
    if (typeof this.props.onSearch === 'function' && suggest.placeId) {
      this.props.onSearch(suggest);
    }
  }

  handleChange(value) {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(value);
    }
  }

  skipSuggest(suggest) {
    return suggest.types.indexOf(this.props.filterSuggestionsBy) === -1;
  }

  render() {
    return (
      <Geosuggest
        className={styles.main}
        autoActivateFirstSuggest={this.props.autoActivateFirstSuggest}
        onSuggestSelect={this.handleSeggestSelect.bind(this)}
        country={this.props.searchCountry}
        skipSuggest={this.skipSuggest.bind(this)}
        onChange={this.handleChange.bind(this)}
        placeholder={this.props.placeholder}
      />
    );
  }
}

export default Search;
