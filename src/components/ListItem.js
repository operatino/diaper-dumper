import React from 'react';
import cx from 'classnames';

import Comments from './Comments';
import styles from './List.css';

class ListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  handleClick(e) {
    e.preventDefault();

    if (typeof this.props.onItemClick === 'function') {
      this.props.onItemClick(this.props);
    }
  }

  render() {
    let classes = cx(
      styles.item,
      {
        [styles.itemSelected]: this.props.selectedItem && this.props.id === this.props.selectedItem
      }
    );
    let linkClasses = cx(
      styles.link
    );

    return (
      <li className={classes}>
        <a href='#' onClick={this.handleClick.bind(this)} className={linkClasses}>
          <strong>{this.props.name}</strong>
          <div>{this.props.city}</div>
          <div>{this.props.address}</div>
        </a>
        <Comments id={this.props.id} />
      </li>
    );
  }
}

export default ListItem;
