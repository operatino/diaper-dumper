import React from 'react';

import styles from './About.css';

class About extends React.Component {
  render() {
    return (
      <div className={styles.main}>
        <p>XebiaLabs Front-end test assigment by Robert Haritonov.</p>
      </div>
    );
  }
}

export default About;
