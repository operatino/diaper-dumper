import Promise from 'promise';
import ApplicationStore from '../stores/ApplicationStore';
import getComments from '../services/getComments';

/**
 * Load comments by ID
 *
 * @param {Object} actionContext
 * @param {Object} payload
 * @param {String} payload.id
 *
 */
export default function LoadComments(actionContext, payload) {
  let applicaitonStore = actionContext.getStore(ApplicationStore);
  let existingComment = applicaitonStore.getCommentsById(payload.id);

  if (existingComment) {
    return Promise.resolve(existingComment);
  }

  return getComments(payload.id)
    .then(data => {
      return actionContext.dispatch('COMMENTS_LOADED', {
        id: payload.id,
        data: data || []
      });
    });
}
