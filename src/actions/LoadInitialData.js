/**
 * Pass map data to store
 *
 * @param {Object} actionContext
 * @param {Array} payload
 *
 */
export default function LoadInitialData(actionContext, payload) {
  return actionContext.dispatch('LOAD_INITIAL_DATA', payload);
}
