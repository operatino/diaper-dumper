import express from 'express';
import getParseComments from '../services/parse/getParseComments';

let router = express.Router();

router.get('/', (req, res) => {
  getParseComments(req.query, (err, data) => {
    if (err) {
      res.sendStatus(500);
      return;
    }

    res.send(data.results);
  });
});

export default router;
