import getParseData from '../services/parse/getParseData';

let cachedData;

function transform(raw) {
  return raw.results.map((item) => {
    return {
      address: item.address,
      city: item.city,
      lat: item.geo.latitude,
      lng: item.geo.longitude,
      name: item.name,
      id: item.objectId
    };
  });
}

function updateCachedData() {
  getParseData((err, json) => {
    if (err) {
      return;
    }

    cachedData = transform(json);
  });
}

// TODO: add update interval when data becomes dynamic
updateCachedData();

export let getData = function () {
  return cachedData;
};

export default function (req, res, next) {
  if (!cachedData) {
    res.sendStatus(404);
    return;
  }

  res.json(cachedData);
};
