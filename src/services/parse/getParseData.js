import fetch from 'isomorphic-fetch';
import urlencode from 'urlencode';

/**
 * Fetch map points for whole netherlands.
 * Supports both callbacks and Promise.
 *
 * @param {Function} [callback]
 *
 * @returns {Promise}
 *
 */
export default function (callback) {
  let where = {
    geo: {
      $within: {
        $box: [{
          __type: 'GeoPoint',
          latitude: 50.77465,
          longitude: 2.80419
        }, {
          __type: 'GeoPoint',
          latitude: 53.53702,
          longitude: 7.55029
        }]
      }
    }
  };
  let whereParam = urlencode(JSON.stringify(where));

  return fetch(`https://api.parse.com/1/classes/location?where=${whereParam}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'X-Parse-Application-Id': 'R8jG6ChCSOGxvB4UWjcMMlEMuloVjoVLo4mS2xkD',
      'X-Parse-REST-API-Key': '8GjcnO3gOdaIE41Nl9Y2juLEQgiNvHVdUM1aZoxF'
    }
  })
  .then((response) => {
    if (response.status > 200) {
      let msg = `Failed fetching data from Parse`;

      console.warn(`gerParseData: ${msg}`);

      if (typeof callback === 'function') {
        callback(msg);
      }

      return;
    }

    return response.json()
      .then((json) => {
        if (typeof callback === 'function') {
          callback(null, json);
        }
      });
  });
};
