import fetch from 'isomorphic-fetch';
import urlencode from 'urlencode';

/**
 * Fetch comments from Parse
 * Supports both callbacks and Promise.
 *
 * @param {Object} payload
 * @param {String} payload.id
 * @param {Function} [callback]
 *
 * @returns {Promise}
 *
 */
export default function (payload, callback) {
  if (!payload.id) {
    if (typeof callback === 'function') {
      let msg = 'No id provided';
      console.warn(`getParseComments: ${msg}`);

      callback(msg);
    }
  }

  let where = {
    location: {
      __type: 'Pointer',
      className: 'location',
      objectId: payload.id
    }
  };
  let whereParam = urlencode(JSON.stringify(where));

  return fetch(`https://api.parse.com/1/classes/review?where=${whereParam}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'X-Parse-Application-Id': 'R8jG6ChCSOGxvB4UWjcMMlEMuloVjoVLo4mS2xkD',
      'X-Parse-REST-API-Key': '8GjcnO3gOdaIE41Nl9Y2juLEQgiNvHVdUM1aZoxF'
    }
  })
  .then((response) => {
    if (response.status > 200) {
      let msg = `Failed fetching comments from Parse`;

      console.warn(`getParseComments: ${msg}`);

      if (typeof callback === 'function') {
        callback(msg);
      }

      return;
    }

    return response.json()
      .then((json) => {
        if (typeof callback === 'function') {
          callback(null, json);
        }
      });
  });
};
