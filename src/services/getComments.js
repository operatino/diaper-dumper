import Promise from 'promise';
import fetch from 'isomorphic-fetch';

/**
 * Fetch comments through local api
 *
 * @param {String} id
 *
 * @returns {Promise}
 *
 */
export default function (id) {
  return fetch(`/api/comments?id=${id}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json'
    }
  })
  .then((response) => {
    if (response.status > 200) {
      let msg = `Failed fetching comments from API`;

      console.warn(`getComments: ${msg}`);

      return Promise.reject(msg);
    }

    return response.json()
      .then((json) => {
        return json;
      });
  });
};
