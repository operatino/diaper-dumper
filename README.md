# Isomorphic Diaper Dumper Search App

```
npm i
npm run dev
open http://localhost:3000/
```

## Build For Production 

```
NODE_ENV=production npm run build
NODE_ENV=production npm start
open http://localhost:3000/
```

## Test

Run unit tests with

```
npm test
npm run test:watch
```

## Other useful commands

```
npm run lint
npm run lint:fix
```

For more commands, check `packages.json` `scripts` section.
