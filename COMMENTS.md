Instructions for running the app are described in the README.md file, and I've also deployed the latest version to https://diaper-dumper.herokuapp.com (first load could be slow, since free heroku instance sleeps if inactive).

Here are few notes on the implementation:
* Within my solution I was achieving the goal of solving the user story itself, instead of focusing on implementation of some less efficient (from my point of view) tech stories.
* Following the limitation of geolocation API, I've chose to use whole Netherlands as "current location" with convenient postal code search bar with autocomplete. I found this approach more flexible for most of the users use cases and considering other similar popular applications for locals like http://www.ah.nl/winkels https://www.t-mobile.nl/winkels. Since my solution is built with SEO in mind (eg isomorphic rendering), combined with common "current location" content could be easier accessible for users coming form the search engines, also bootstriping time of the app is times faster than with alternative solutions.
* I've chose other data loading strategy, since considering the amount of total dumpsters is really low, doing a call per coordinates chunk is an overhead. Instead, I've optimzed and cached the payload and fetch data for whole Netherlands all together, which with gzip is only about 2kb, and will work well even with 10 times more data.

As user stories were quite extended, as mentioned at the start, I intentionally skipped additional stories to focus on the baseline feature set, development environment, and tests set up. If I would have more time, additionally to stories with auth I would implement next features:

* Optional button that triggers geo position look up, for users that are looking nearby dumpsters from the current location they are right now.
* Integration tests with selenium (webdriver.io), since unit tests does not give enough confidence in the app stability. Also would finish the unit test coverage of services, actions and stores.
* Would add routing mechanism connected to postal code lookup, so users could bookmark the search and share it with others.
* Would add React props validation, eg type checking
* And of course, better UI design should be applied in consultation with web designers.
